import React, { Component } from 'react'
import Header from '../../components/Header';

import { listNotes, saveNotes } from '../../services/serviceNotes'

import './style.css'

export default class Notes extends Component {
  
  state = {
    notes: [],
    loading: true,
    loadingSave: false,
    text: ''
  }

  async componentDidMount () {
    this.setState({
      notes: await listNotes(),
      loading: false
    })
  }

  newNote = () => {
    this.setState({
      new: true
    })
  }

  editText = (event) => {
    this.setState({
      text: event.target.value
    })
  }
  
  editNote = (index) => {
    this.setState({
      montar: true,
      text: this.state.notes[index].text
    })
  }

  

  saveNote = async () => {
    this.setState({
      loadingSave: true
    })
    await saveNotes(this.state.text)
    this.setState({
      notes: await listNotes(),
      loadingSave: false,
      new: false,
      text: ''
    })
  }
  
  delNote = (index) => {
    delete this.state.notes[index]
    this.setState({
        notes:this.state.notes
    })
}

  render() {
    return (
      <div>
        <Header/>
        <div className='content'>
          
          <div className='filters'>
            <div className='left'>
              <input type='search' placeholder='Procurar...' />
              <button onClick={this.enconNote}>Procurar</button>
            </div>
            <div className='right'>
              <button onClick={this.newNote}>Nova nota</button>
            </div>
          </div>

          <hr />

          <div className='list'>
            { this.state.loading && (
              <div className='item item-empty'>
                Carregando anotações...
              </div>
            ) }
            { !this.state.notes.length && !this.state.loading && (
              <div className='item item-empty'>
                Nenhuma nota criada até o momento!
              </div>
            ) }
            { this.state.new && (
              <div className='item'>
                { !this.state.montar && (<input onChange={this.editText} className='left' type='text' placeholder='Digite sua anotação' />)}
                { this.state.montar && (<input onChange={this.editText} className='left' type='text' placeholder='Digite sua anotação' defaultValue={this.state.text} />)}
                <div className='right'>
                  { !this.state.loadingSave && (<button onClick={this.saveNote}>Salvar</button>) }
                  { this.state.loadingSave && (<span>Salvando...</span>) }
                </div>
              </div>
            )}

            {this.state.notes.map((note,index) => (
               <div key={index}>
              <div className='item'>
                {note.text} 
                <div className='right'>
                  { note.id && (<button onClick={() => {if (window.confirm("vai excluir?"))this.delNote(index)}}>Excluir</button>) }
                  { note.id && (<button onClick={() => {this.editNote(index) ;this.delNote(index) ; this.newNote() }}>Editar</button>)}
                </div>
              </div>
            </div>
            ))}
          </div>
        </div>
      </div>
    )
  }
}
